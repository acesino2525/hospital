package trabajos.practica.ejercicio2;

public class Butaca {
    private int fila;
    private int numeracion;

    public Butaca(int fila, int numeracion){

         this.fila = fila;
         this.numeracion = numeracion;

    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getNumeracion() {
        return numeracion;
    }

    public void setNumeracion(int numeracion) {
        this.numeracion = numeracion;
    }
}
