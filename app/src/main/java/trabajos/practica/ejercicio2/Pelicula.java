package trabajos.practica.ejercicio2;

public class Pelicula {
    private String genero;
    private String titulo;
    private int duracion;
    private int edad_minima;
    private String actores;
    private String director;

public Pelicula(String genero, String titulo, int duracion, int edad_minima,
String actores, String director){

    this.actores = actores;
    this.director = director;
    this.duracion = duracion;
    this.edad_minima = edad_minima;
    this.titulo = titulo;
    this.genero = genero;

  } 

}
