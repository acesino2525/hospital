package trabajos.practica.ejercicio2;

public class Entrada {
    
    private int nro_ticket;
    private int num_funcion;
    private int precio;
    private String fecha_venta;
    private String nombre_pelicula;
    private String fecha_funcion;
    private String hora_funcion;
    private String clasificacion_peli;

    public Entrada(int nro_ticket, int num_funcion, int precio, String fecha_funcion,
     String fecha_venta,String hora_funcion, String clasificacion_peli){

        this.clasificacion_peli = clasificacion_peli;
        this.fecha_funcion = fecha_funcion;
        this.fecha_venta = fecha_venta;
        this.hora_funcion = hora_funcion;
        this.nombre_pelicula = nombre_pelicula;
        this.nro_ticket = nro_ticket;
        this.num_funcion = num_funcion;
        this.precio = precio;
     
    }

}
