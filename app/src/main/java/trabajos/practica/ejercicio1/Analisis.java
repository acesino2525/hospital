package trabajos.practica.ejercicio1;

import java.time.LocalDate;

public class Analisis {
    private String tipo;
    private int numero;
    private LocalDate fecha;
    private String resultado;

    public Analisis(String tipo, int numero, LocalDate fecha, String resultado){

        this.fecha = fecha;
        this.numero = numero;
        this.resultado = resultado;
        this.tipo = tipo;

    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }


}
