package trabajos.practica.ejercicio1;

public class Domicilio {
    private int numero;
    private String calle;
    private String localidad;

    public int getNumero() {
      return numero;
    }

    public void setNumero(int numero) {
      this.numero = numero;
    }

    public String getCalle() {
      return calle;
    }

    public void setCalle(String calle) {
      this.calle = calle;
    }

    public String getLocalidad() {
      return localidad;
    }

    public void setLocalidad(String localidad) {
      this.localidad = localidad;
    }

    public Domicilio(String calle, String localidad, int numero){

      this.calle = calle;
      this.localidad = localidad;
      this.numero = numero;

    }
    
}
